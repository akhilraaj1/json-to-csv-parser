#! /usr/bin/env python
import json, csv
import os.path
from os import path
file_exists=path.exists('data.csv')

with open('results.json') as json_file:
    data = json.load(json_file)
output_list=[data["info"]["timestamp"],data["info"]["system"],data["results"][0]["fps"]]
#print(output_list)
if file_exists:
    with open('data.csv','r+') as csv_file:
        reversed_list = list (reversed(list(csv_file)))
        last_element = reversed_list[0].split(",")
        if last_element[0] == output_list[0]:
            print("Please update the results.json file. It seems that you are using the old file")
        else:
            csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(output_list)
else:
    with open('data.csv','w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(["timestamp","system","fps"])
        csv_writer.writerow(output_list)
    

