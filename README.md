# JSON TO CSV Parser
JSON to CSV parser used to log the logs from the JSON to CSV.

## How it looks
```bash
timestamp,system,fps
23-09-2020 10:25:26,Linux sujay-VirtualBox 5.4.0-47-generic #51~18.04.1-Ubuntu SMP Sat Sep 5 14:35:50 UTC 2020 x86_64,333.8655517480304
```

## Usage
Copy your .json file and rename into results.json and use the following command to generate the csv.
```bash
python parser.py
```
If you have existing csv (data.csv) just copy the file and just run the command as before. It can append to the existing file.


